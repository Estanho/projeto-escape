#include <QApplication>
#include <iostream>

#include "includes/includes.h"
#include "GUI-build/gui.h"

#include "Escalonador/FIFO.h"
#include "Paginacao/DumbFifo.h"

ESCAPE_START

using namespace std;

/*
crie seu escalonador como uma classe,
herdando de Escalonador, e implemente o
m�todo escalonar() de acordo com o algoritmo
pedido. Ent�o inclua o .h da classe aqui,
e altere a cria��o da VM abaixo com o nome
do seu escalonador.
*/

VM* vm = NULL;
void iniciar();

void montar();

bool insert(std::string entrada);

int main(int argc, char* argv[]) {
    MAIN_BEGIN

    SET_PROC_WINDOW

    
    Janela::p_Instance()->set_Read(PARssembly_Reader::ReadProc);
    Janela::p_Instance()->set_UnRead(PARssembly_Reader::UnReadProc);
    Janela::p_Instance()->set_Insert(insert);
    Janela::p_Instance()->set_MountVm(montar);
    w.set_Run(iniciar);

    ESCAPE_RUN

    return 0;
}

bool insert(std::string entrada) {
    if(vm->criaProcesso(entrada) == 0) {
	  return false;
    }
    return true;
}

void iniciar() {
	if(!vm) throw Exception(__LINE__,__FILE__, "Voc� n�o pode rodar o projeto sem montar a VM!!!");
	
	switch(Janela::p_Instance()->get_Algorithm()) {
		case ALG_FIFO:
			vm->set_Escalonador(new FIFO());
			break;
		case ALG_SJF:
			//vm->set_Escalonador(new SJF()); //� medida que for implementando os algoritmos, desbloqueie eles aqui.
			break;			
		case ALG_ROUND:
			//vm->set_Escalonador(new ROUND());
			break;			
		case ALG_MFQ:
			//vm->set_Escalonador(new MFQ());
			break;
		default:
			throw Exception(__LINE__,__FILE__, "Voc� deve escolher um algoritmo para rodar!");
			break;
	}
	
	void* vm_iniciar(void*);

	pthread_t startvm;

	pthread_create(&startvm, NULL, vm_iniciar, NULL);
}

void* vm_iniciar(void*){
	vm->iniciar();

	pthread_exit(NULL);
}

void montar() {
	if(vm) delete vm;
	
    vm = new VM();
    new DumbFifo(  vm -> get_Memoria() );
}