#ifndef PARSSEMBLY_H_INCLUDED
#define PARSSEMBLY_H_INCLUDED

#include "includes.h"
#include "../GUI/gui.h"

using namespace std;

struct Dummy_Proc {
	char modulo[64]; //Nome do arquivo
	bool deletable;
	std::vector<Comando> _ComList; //Lista de comandos
	std::map<variavel, unsigned>* vars; //Variaveis com endereço VIRTUAL
	char * data;
	unsigned tam_data;
	unsigned tam_vars;

   	std::map<std::string, unsigned int> LabelPc;
	
	~Dummy_Proc()///CUIDADO COM ESSA CLASSE, para evitar vasão de memoria essa classe tem um boleado que diz se a memoria alocada para essa estruturada
	///Deve ou não ser deletada, ( deletable ), caso ele seja verdade delete irá rodar na saida de escopo da variavel. Por padrão o contrutor deixa ela não deletable
	///O deixe verdade apenas em 1 elemento caso haja copia e garanta que esse seja o último a sair de escopo.
	{
		if( deletable)
		{
			delete vars;
			free(data);
		}    
	}
	Dummy_Proc()
	{
	  deletable = false;
	}
};

class ProcBuffer {
private:
    map<std::string, Dummy_Proc> proc;

    //Section
    bool section(
        char*
    );
    bool data(
        char*
    );
    bool bss(
        char*
    );
    bool text(
        char*
    );

    //Instructions

    ///Arithmetic
    bool _mov(
        char*
    );
    bool _movi(
        char*
    );
    bool _der(
        char*
    );
    bool _add(
        char*
    );
    bool _sub(
        char*
    );
    bool _mul(
        char*
    );
    bool _imul(
        char*
    );
    bool _div(
        char*
    );
    bool _idiv(
        char*
    );
    bool _and(
        char*
    );
    bool _or(
        char*
    );
    bool _xor(
        char*
    );
    bool _not(
        char*
    );
    ///Functional
    bool _push(
        char*
    );
    bool _pop(
        char*
    );
    bool _call(
        char*
    );
    bool _ret(
        char*
    );
    ///Program Flux
    bool _cmp(
        char*
    );
    bool _jg(
        char*
    );
    bool _jge(
        char*
    );
    bool _jl(
        char*
    );
    bool _jle(
        char*
    );
    bool _je(
        char*
    );
    bool _jne(
        char*
    );
    bool _ja(
        char*
    );
    bool _jae(
        char*
    );
    bool _jb(
        char*
    );
    bool _jbe(
        char*
    );
    bool _jmp(
        char*
    );
    ///IO
    bool _print(
        char*
    );
    ///EXTRA
    bool _label(
        char*
    );
    ///PARALLEL
    bool _sat(
        char*
    );
    bool _eat(
        char*
    );
    bool _proc(
        char*
    );
    bool _sproc(
        char*
    );
    bool _eproc(
        char*
    );

public:
    bool AddProc(
        std::string
    );

    bool GetProc(
        std::string,
        Processo* ,
        Memoria*
    );

    void DelProc(
        std::string
    );
};


class PARssembly_Reader {
private:
    static ProcBuffer ps;

public:
    static void Initialize(
    );
    static bool ReadProc(
        std::string
    );
    static void UnReadProc(
        std::string
    );
    static ProcBuffer* getPbuffer(
    );
};

#endif // PARSSEMBLY_H_INCLUDED
