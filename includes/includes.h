#ifndef INCLUDES_H_INCLUDED
#define INCLUDES_H_INCLUDED

/** DECLARA��ES P�BLICAS **/
class Escalonador;
typedef unsigned int PID;
typedef char* arquivo;
class Mutex;
class VM;
class Processo;
typedef unsigned int PID;
typedef struct data DATA;
typedef struct stdoutchar stdoutChar;
typedef int num_buffer;
class Buffer;
typedef int num_buffer;

#include <inttypes.h>
#include <iostream>
#include <cstdlib>
#include <map>
#include <string>
#include <queue>
#include <stack>
#include <vector>
#include <algorithm>
#include <sstream>
#include <pthread.h>

#include "Memoria.h"
#include "MemoriaFisica.h"
#include "MemoriaVirtual.h"
#include "Comando.h"
#include "Estado.h"
#include "Processo.h"
#include "Escalonador.h"
#include "VM.h"
#include "PARssembly.h"
#include "FileSystem.h"


#endif // INCLUDES_H_INCLUDED
