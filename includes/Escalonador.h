#ifndef ESCALONADOR_H_INCLUDED
#define ESCALONADOR_H_INCLUDED

#include "includes.h"

class Escalonador {
    friend class VM;

    private:
        unsigned int quantum;
        std::map<PID, Processo*>* mapaProcesso;
        std::queue<PID>* novosProcessos;

    protected:
        virtual Processo* escalonar() = 0;
        virtual void Inicializar() = 0;

        void set_quantum(
            unsigned int
        );

        unsigned int get_quantum(
        );

        const std::map<PID, Processo*>* get_mapaProcesso(
        );

        const std::queue<PID>* get_novosProcessos(
        );

        virtual ~Escalonador();
};



#endif // ESCALONADOR_H_INCLUDED
