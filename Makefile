CXX = g++ -Wall -O2
HEADERS = -I./GUI -I./includes -I/usr/include/qt4/ -I/usr/include/qt4/QtGui/ -I/usr/include/QtGui
LIBS = -L/usr/lib/x86_64-linux-gnu ./GUI-build/libGUI.a -lQtCore -lQtGui -lpthread

SOURCE = Escalonador/Escalonador.cpp Processo/Processo.cpp VM/FileSystem.cpp VM/MemoriaVirtual.cpp VM/MemoriaFisica.cpp VM/Memoria.cpp VM/PARssembly.cpp VM/TOOLS.cpp  
MEMORIA = Paginacao/DumbFifo.cpp # Paginacao/Fifo.cpp 
ESCALONADOR = Escalonador/FIFO.cpp

all:
	cd GUI-build && qmake -project && qmake GUI.pro && $(MAKE) && cd .. && ${CXX} ${HEADERS} ${SOURCE} ${MEMORIA} ${ESCALONADOR} VM/VM.cpp main.cpp ${LIBS} -o ESCAPE
nw: #no warning
	cd GUI-build && qmake -project && qmake GUI.pro && $(MAKE) && cd .. && ${CXX} -w ${HEADERS} ${SOURCE} ${MEMORIA} ${ESCALONADOR} VM/VM.cpp main.cpp ${LIBS} -o ESCAPE
ng: #not compile GUI
	${CXX} ${HEADERS} ${SOURCE} ${MEMORIA} ${ESCALONADOR} VM/VM.cpp main.cpp ${LIBS} -o ESCAPE
ngnw:#don't compile GUI and not warning
	${CXX} -w ${HEADERS} ${SOURCE} ${MEMORIA} ${ESCALONADOR} VM/VM.cpp main.cpp ${LIBS} -o ESCAPE
nwng: #don't compile GUI and not warning 
	make ngnw
clean: #clean binary FileSystemes
	rm ESCAPE ; rm GUI-build/*.o ; rm GUI-build/*.a
debug: #compile in debug-mode.
	${CXX} -g ${HEADERS} ${SOURCE} ${MEMORIA} ${ESCALONADOR} VM/VM.cpp main.cpp ${LIBS} -o ESCAPE 
