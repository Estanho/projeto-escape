
Projeto ESCAPE
==============

--------------------------------------------
# Lembrete: SEMPRE DAR CLEAN AO DAR COMMIT #
--------------------------------------------

Autores:
---------------
- 	Guilherme Caminha	(gpkc@cin.ufpe.br)
-	Djeefther Souza		(dsa2@cin.ufpe.br)

Co-Autores:
---------------------
-	Rafael Carvalho		(rtmc@cin.ufpe.br)
-	Rodrigo Lopes		(rlc2@cin.ufpe.br)
-	Pedro Dias		(phds@cin.ufpe.br)

Como usar:
-----------------------
É necessário ter as bibliotecas do Qt instaladas.
No ubuntu você pode fazer:  
$ sudo apt-get install libqt4-* qtcreator  
  
Tendo feito isso, você já pode compilar e executar o projeto:  
$ make  
$ ./ESCAPE  

TODO
--------------------

1. Fazer a VM trabalhar em conjunto com a nova unidade de memória
2. Modificar e melhorar o parser (Adicionar erros e etc)
