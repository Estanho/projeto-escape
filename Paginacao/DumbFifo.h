#include "MemoriaVirtual.h"
#include "../includes/TOOLS.h"


class DumbFifo : public MemoriaVirtual
{
	unsigned num_paginas;
public:
	
	DumbFifo( Memoria * memoria );
	~DumbFifo(){};

	unsigned access_4( unsigned adress); 
	void access_4( unsigned adress, unsigned value );
	bool newPages( unsigned& paginaVirtual, unsigned numeroPaginas );
	void deletePages( unsigned paginaVirtual , unsigned numeroPaginas );
	bool statusPage( unsigned& paginaFisica , unsigned paginaVirtual );

};