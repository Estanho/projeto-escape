#include "gui.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <iostream>

#include "qt_gui_handler.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QT_GUI_HANDLER::mui = NULL;

    QT_GUI_HANDLER::setCenterOfApplication(this, false);

    ui->addProc->setEnabled(false);

    connect(

        ui->Terminal->model(),

        SIGNAL(rowsInserted ( const QModelIndex &, int, int ) ),

        ui->Terminal,

        SLOT(scrollToBottom ())

    );

}

MainWindow::~MainWindow()
{
    delete QT_GUI_HANDLER::mui;
    delete ui;
}

void MainWindow::exit() {
    close();
    qApp->exit();
}

void MainWindow::on_addProc_clicked() {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "PARnasm", tr("Files (*.*)"));

    if(fileName.size() < 1) {
        return;
    }

    QString::Iterator it = fileName.end();
    QString file;

    it--;
    while(*it != QChar('/')) {
        file.push_front(*it);
        it--;
    }

    std::string path = "PARnasm/" + file.toStdString();

    if(!(QT_GUI_HANDLER::CBReadFunction(path))) {
        return;
    }

    ui->procList->addItem(file);
}

void MainWindow::on_remProc_clicked() {
    if(ui->procList->selectedItems().size()) {
        QT_GUI_HANDLER::CBUnReadFunction(ui->procList->selectedItems().first()->text().toStdString());
        delete ui->procList->selectedItems().first();
    }
}

void MainWindow::inc_Procs(){
    ui->numProcs->setText(QString::number(ui->numProcs->text().toInt()+1));
}
void MainWindow::dec_Procs(){
    ui->numProcs->setText(QString::number(ui->numProcs->text().toInt()-1));
}

void MainWindow::on_procList_doubleClicked(const QModelIndex &index) {
    QString item = ((QListWidgetItem*)index.internalPointer())->text();

    std::string path = "PARnasm/" + item.toStdString();

    if(QT_GUI_HANDLER::CBInsertFunction(path)) {
	  ui->runList->addItem(item);
	  inc_Procs();
    }
}

bool MainWindow::set_Terminal_line(std::string str) {

    ui->Terminal->addItem(QString(str.c_str()));

   // if( ui->actionAutoScroll->isChecked() ) {
     //   ui->Terminal->scrollToBottom();
    //}


    if(ui->Terminal->count() >= MAX_LINES) {
        ui->Terminal->takeItem(0);
    }


    return true;
}

void MainWindow::on_Terminal_textChanged() {
    /*QTextCursor c_old = ui->Terminal->textCursor();

    QTextCursor c = ui->Terminal->textCursor();

    c.movePosition(QTextCursor::End);

    if( ui->actionAutoScroll->isChecked() ) {
        c.movePosition(QTextCursor::End);
        ui->Terminal->setTextCursor(c);
        c_old.movePosition(QTextCursor::End);
    }*/

}

int MainWindow::get_Algorithm() {
    ui->algoritmo->setDisabled(true);
    if(ui->algoritmo->currentText() == QString("FIFO")) {
        return ALG_FIFO;
    }
    else if(ui->algoritmo->currentText() == QString("SJF")) {
        return ALG_SJF;
    }
    else if(ui->algoritmo->currentText() == QString("Round-Robin")) {
        return ALG_ROUND;
    }
    else if(ui->algoritmo->currentText() == QString("MFQ")) {
        return ALG_MFQ;
    }
    ui->algoritmo->setDisabled(false);
    return ALG_INVALID;
}

void MainWindow::on_actionSair_triggered() {
    exit();
}

void MainWindow::on_actionRodar_triggered() {
    if(QString::number(ui->numProcs->text().toInt()) == QString("0")) return;

    ui->MontarVm->setEnabled(false);
    ui->Memoria->setEnabled(false);
    ui->addProc->setEnabled(false);
    ui->remProc->setEnabled(false);

    QT_GUI_HANDLER::CBRunFunction();
}

void MainWindow::on_Memoria_clicked()
{
    if(!QT_GUI_HANDLER::mui) {
        QT_GUI_HANDLER::mui = new MemWindow(this);
        QT_GUI_HANDLER::mui->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    }
    QT_GUI_HANDLER::mui->show();
}

int MainWindow::get_tamRam() {
    if(!QT_GUI_HANDLER::mui) return PADRAO_REAL;
    return QT_GUI_HANDLER::mui->get_tamRam();
}

int MainWindow::get_tamVir() {
    if(!QT_GUI_HANDLER::mui) return PADRAO_VIRTUAL;
    return QT_GUI_HANDLER::mui->get_tamVir();
}

int MainWindow::get_pageSize() {
    if(!QT_GUI_HANDLER::mui) return PADRAO_PAGINA;
    return QT_GUI_HANDLER::mui->get_pageSize();
}

void MainWindow::set_Read(bool (r_f)(std::string)) {
    QT_GUI_HANDLER::CBReadFunction = r_f;
}
void MainWindow::set_UnRead(void (u_f)(std::string)) {
    QT_GUI_HANDLER::CBUnReadFunction = u_f;
}
void MainWindow::set_Insert(bool (i_f)(std::string)) {
    QT_GUI_HANDLER::CBInsertFunction = i_f;
}
void MainWindow::set_Run(void (r_f)()) {
    QT_GUI_HANDLER::CBRunFunction = r_f;
}
void MainWindow::set_MountVm(void (r_vm)()) {
    QT_GUI_HANDLER::CBMountVm = r_vm;
}

void MainWindow::on_MontarVm_clicked() {
    QT_GUI_HANDLER::CBMountVm();
    ui->addProc->setEnabled(true);
    ui->remProc->setEnabled(true);
    ui->Memoria->setEnabled(false);
}

void MainWindow::removeProc(const char* item) {
    QList<QListWidgetItem*> itm = ui->runList->findItems(QString(item), 0);
    if(!itm.empty()) {
        ui->runList->takeItem(ui->runList->row(itm.front()));
        dec_Procs();
    }
    if(QString::number(ui->numProcs->text().toInt()) == QString("0")) { //TERMINOU
        ui->addProc->setEnabled(false);
        ui->Memoria->setEnabled(true);
        ui->MontarVm->setEnabled(true);
        ui->algoritmo->setDisabled(false);
    }
}

void MainWindow::on_Liberar_clicked() {
    //ui->Terminal->clear();
}

void MainWindow::on_Terminal_itemEntered(QListWidgetItem *) {

}
