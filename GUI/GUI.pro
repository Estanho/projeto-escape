#-------------------------------------------------
#
# Project created by QtCreator 2013-05-25T01:35:33
#
#-------------------------------------------------

TARGET = GUI
TEMPLATE = lib
CONFIG += staticlib

SOURCES += gui.cpp \
    memwindow.cpp \
    opcoes.cpp \
    qt_gui_handler.cpp

HEADERS += gui.h \
    memwindow.h \
    opcoes.h \
    qt_gui_handler.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

FORMS += \
    mainwindow.ui \
    memwindow.ui \
    opcoes.ui
