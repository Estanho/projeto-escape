/********************************************************************************
** Form generated from reading UI file 'opcoes.ui'
**
** Created: Fri Aug 23 16:34:18 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPCOES_H
#define UI_OPCOES_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_Opcoes
{
public:
    QDialogButtonBox *buttonBox;
    QLineEdit *TamVir;
    QLabel *nump;
    QLabel *tamp;
    QLineEdit *PageSize;
    QLabel *tamr;
    QLineEdit *TamRam;
    QLabel *label;

    void setupUi(QDialog *Opcoes)
    {
        if (Opcoes->objectName().isEmpty())
            Opcoes->setObjectName(QString::fromUtf8("Opcoes"));
        Opcoes->resize(222, 300);
        buttonBox = new QDialogButtonBox(Opcoes);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 260, 181, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        TamVir = new QLineEdit(Opcoes);
        TamVir->setObjectName(QString::fromUtf8("TamVir"));
        TamVir->setGeometry(QRect(10, 60, 71, 31));
        nump = new QLabel(Opcoes);
        nump->setObjectName(QString::fromUtf8("nump"));
        nump->setGeometry(QRect(10, 40, 191, 21));
        tamp = new QLabel(Opcoes);
        tamp->setObjectName(QString::fromUtf8("tamp"));
        tamp->setGeometry(QRect(10, 90, 191, 21));
        PageSize = new QLineEdit(Opcoes);
        PageSize->setObjectName(QString::fromUtf8("PageSize"));
        PageSize->setGeometry(QRect(10, 110, 91, 31));
        tamr = new QLabel(Opcoes);
        tamr->setObjectName(QString::fromUtf8("tamr"));
        tamr->setGeometry(QRect(10, 140, 181, 21));
        TamRam = new QLineEdit(Opcoes);
        TamRam->setObjectName(QString::fromUtf8("TamRam"));
        TamRam->setGeometry(QRect(10, 160, 91, 31));
        label = new QLabel(Opcoes);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 0, 171, 21));

        retranslateUi(Opcoes);
        QObject::connect(buttonBox, SIGNAL(accepted()), Opcoes, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Opcoes, SLOT(reject()));

        QMetaObject::connectSlotsByName(Opcoes);
    } // setupUi

    void retranslateUi(QDialog *Opcoes)
    {
        Opcoes->setWindowTitle(QApplication::translate("Opcoes", "Op\303\247\303\265es de Mem\303\263ria", 0, QApplication::UnicodeUTF8));
        nump->setText(QApplication::translate("Opcoes", "Tamanho da Mem\303\263ria Virtual", 0, QApplication::UnicodeUTF8));
        tamp->setText(QApplication::translate("Opcoes", "Tamanho das p\303\241ginas", 0, QApplication::UnicodeUTF8));
        tamr->setText(QApplication::translate("Opcoes", "Tamanho da Mem\303\263ria RAM", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Opcoes", "Dados em bytes", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Opcoes: public Ui_Opcoes {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPCOES_H
