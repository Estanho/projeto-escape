/********************************************************************************
** Form generated from reading UI file 'memwindow.ui'
**
** Created: Fri Aug 23 16:34:18 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MEMWINDOW_H
#define UI_MEMWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTableWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MemWindow
{
public:
    QAction *actionFechar;
    QWidget *centralwidget;
    QLabel *l_real;
    QTableWidget *Real;
    QPushButton *Opcoes;
    QTableWidget *Registradores;
    QComboBox *Processos;
    QLabel *l_regs;
    QLabel *l_proc;
    QTableWidget *Virtual;
    QLabel *l_virtual;
    QTableWidget *TabelaPaginas;
    QLabel *l_table;
    QFrame *line;
    QFrame *line_2;
    QMenuBar *menubar;
    QMenu *menuMenu;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MemWindow)
    {
        if (MemWindow->objectName().isEmpty())
            MemWindow->setObjectName(QString::fromUtf8("MemWindow"));
        MemWindow->resize(890, 600);
        MemWindow->setMinimumSize(QSize(690, 600));
        MemWindow->setMaximumSize(QSize(890, 600));
        actionFechar = new QAction(MemWindow);
        actionFechar->setObjectName(QString::fromUtf8("actionFechar"));
        actionFechar->setCheckable(false);
        centralwidget = new QWidget(MemWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        l_real = new QLabel(centralwidget);
        l_real->setObjectName(QString::fromUtf8("l_real"));
        l_real->setGeometry(QRect(580, 10, 91, 21));
        Real = new QTableWidget(centralwidget);
        if (Real->columnCount() < 2)
            Real->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        Real->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        Real->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        Real->setObjectName(QString::fromUtf8("Real"));
        Real->setGeometry(QRect(580, 30, 301, 511));
        Real->setMinimumSize(QSize(0, 0));
        Opcoes = new QPushButton(centralwidget);
        Opcoes->setObjectName(QString::fromUtf8("Opcoes"));
        Opcoes->setGeometry(QRect(120, 10, 91, 21));
        Registradores = new QTableWidget(centralwidget);
        if (Registradores->columnCount() < 1)
            Registradores->setColumnCount(1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        Registradores->setHorizontalHeaderItem(0, __qtablewidgetitem2);
        if (Registradores->rowCount() < 8)
            Registradores->setRowCount(8);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        Registradores->setVerticalHeaderItem(0, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        Registradores->setVerticalHeaderItem(1, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        Registradores->setVerticalHeaderItem(2, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        Registradores->setVerticalHeaderItem(3, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        Registradores->setVerticalHeaderItem(4, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        Registradores->setVerticalHeaderItem(5, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        Registradores->setVerticalHeaderItem(6, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        Registradores->setVerticalHeaderItem(7, __qtablewidgetitem10);
        Registradores->setObjectName(QString::fromUtf8("Registradores"));
        Registradores->setGeometry(QRect(364, 350, 161, 192));
        Processos = new QComboBox(centralwidget);
        Processos->setObjectName(QString::fromUtf8("Processos"));
        Processos->setGeometry(QRect(364, 290, 161, 31));
        l_regs = new QLabel(centralwidget);
        l_regs->setObjectName(QString::fromUtf8("l_regs"));
        l_regs->setGeometry(QRect(364, 330, 111, 21));
        l_proc = new QLabel(centralwidget);
        l_proc->setObjectName(QString::fromUtf8("l_proc"));
        l_proc->setGeometry(QRect(364, 270, 65, 21));
        Virtual = new QTableWidget(centralwidget);
        if (Virtual->columnCount() < 2)
            Virtual->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        Virtual->setHorizontalHeaderItem(0, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        Virtual->setHorizontalHeaderItem(1, __qtablewidgetitem12);
        Virtual->setObjectName(QString::fromUtf8("Virtual"));
        Virtual->setGeometry(QRect(10, 30, 301, 511));
        Virtual->setMaximumSize(QSize(999, 511));
        l_virtual = new QLabel(centralwidget);
        l_virtual->setObjectName(QString::fromUtf8("l_virtual"));
        l_virtual->setGeometry(QRect(10, 10, 101, 21));
        TabelaPaginas = new QTableWidget(centralwidget);
        if (TabelaPaginas->columnCount() < 2)
            TabelaPaginas->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        TabelaPaginas->setHorizontalHeaderItem(0, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        TabelaPaginas->setHorizontalHeaderItem(1, __qtablewidgetitem14);
        TabelaPaginas->setObjectName(QString::fromUtf8("TabelaPaginas"));
        TabelaPaginas->setGeometry(QRect(325, 30, 241, 241));
        l_table = new QLabel(centralwidget);
        l_table->setObjectName(QString::fromUtf8("l_table"));
        l_table->setGeometry(QRect(344, 10, 121, 21));
        line = new QFrame(centralwidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(556, 30, 20, 511));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        line_2 = new QFrame(centralwidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setGeometry(QRect(311, 30, 31, 511));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);
        MemWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MemWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 890, 29));
        menuMenu = new QMenu(menubar);
        menuMenu->setObjectName(QString::fromUtf8("menuMenu"));
        MemWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MemWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MemWindow->setStatusBar(statusbar);

        menubar->addAction(menuMenu->menuAction());
        menuMenu->addAction(actionFechar);

        retranslateUi(MemWindow);

        QMetaObject::connectSlotsByName(MemWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MemWindow)
    {
        MemWindow->setWindowTitle(QApplication::translate("MemWindow", "Mem\303\263ria", 0, QApplication::UnicodeUTF8));
        actionFechar->setText(QApplication::translate("MemWindow", "Fechar", 0, QApplication::UnicodeUTF8));
        l_real->setText(QApplication::translate("MemWindow", "Mem\303\263ria Real", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem = Real->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MemWindow", "Endere\303\247o", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem1 = Real->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MemWindow", "Valor", 0, QApplication::UnicodeUTF8));
        Opcoes->setText(QApplication::translate("MemWindow", "Op\303\247\303\265es", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem2 = Registradores->horizontalHeaderItem(0);
        ___qtablewidgetitem2->setText(QApplication::translate("MemWindow", "VAL", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem3 = Registradores->verticalHeaderItem(0);
        ___qtablewidgetitem3->setText(QApplication::translate("MemWindow", "EAX", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem4 = Registradores->verticalHeaderItem(1);
        ___qtablewidgetitem4->setText(QApplication::translate("MemWindow", "EBX", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem5 = Registradores->verticalHeaderItem(2);
        ___qtablewidgetitem5->setText(QApplication::translate("MemWindow", "ECX", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem6 = Registradores->verticalHeaderItem(3);
        ___qtablewidgetitem6->setText(QApplication::translate("MemWindow", "EDX", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem7 = Registradores->verticalHeaderItem(4);
        ___qtablewidgetitem7->setText(QApplication::translate("MemWindow", "EEX", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem8 = Registradores->verticalHeaderItem(5);
        ___qtablewidgetitem8->setText(QApplication::translate("MemWindow", "EFX", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem9 = Registradores->verticalHeaderItem(6);
        ___qtablewidgetitem9->setText(QApplication::translate("MemWindow", "EGX", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem10 = Registradores->verticalHeaderItem(7);
        ___qtablewidgetitem10->setText(QApplication::translate("MemWindow", "EHX", 0, QApplication::UnicodeUTF8));
        l_regs->setText(QApplication::translate("MemWindow", "Registradores", 0, QApplication::UnicodeUTF8));
        l_proc->setText(QApplication::translate("MemWindow", "Processo", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem11 = Virtual->horizontalHeaderItem(0);
        ___qtablewidgetitem11->setText(QApplication::translate("MemWindow", "Endere\303\247o", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem12 = Virtual->horizontalHeaderItem(1);
        ___qtablewidgetitem12->setText(QApplication::translate("MemWindow", "Valor", 0, QApplication::UnicodeUTF8));
        l_virtual->setText(QApplication::translate("MemWindow", "Mem\303\263ria Virtual", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem13 = TabelaPaginas->horizontalHeaderItem(0);
        ___qtablewidgetitem13->setText(QApplication::translate("MemWindow", "P\303\241gina", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem14 = TabelaPaginas->horizontalHeaderItem(1);
        ___qtablewidgetitem14->setText(QApplication::translate("MemWindow", "Moldura", 0, QApplication::UnicodeUTF8));
        l_table->setText(QApplication::translate("MemWindow", "Tabela de P\303\241ginas", 0, QApplication::UnicodeUTF8));
        menuMenu->setTitle(QApplication::translate("MemWindow", "Menu", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MemWindow: public Ui_MemWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MEMWINDOW_H
