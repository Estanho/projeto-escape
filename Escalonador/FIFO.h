#ifndef FIFO_H_INCLUDED
#define FIFO_H_INCLUDED

#include "includes.h"

class FIFO : public Escalonador {
	private:
		std::queue<Processo*> processos;
    public:
    	FIFO()	{};
    	~FIFO() {};

        Processo* escalonar();
        void Inicializar();
};

#endif // FIFO_H_INCLUDED
