#include "../includes/Escalonador.h"

void Escalonador::set_quantum(unsigned int quantum){

    this->quantum = quantum;
}

unsigned int Escalonador::get_quantum(){
    return this->quantum;
}

const std::map<PID, Processo*>* Escalonador::get_mapaProcesso(){
    return this->mapaProcesso;
}

const std::queue<PID>* Escalonador::get_novosProcessos(){
    return this->novosProcessos;
}

Escalonador::~Escalonador() {}
