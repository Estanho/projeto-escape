#ifndef OPCOES_H
#define OPCOES_H

#include <QDialog>

#define PADRAO_REAL 8192
#define PADRAO_VIRTUAL 16384
#define PADRAO_PAGINA 256

namespace Ui {
class Opcoes;
}

class Opcoes : public QDialog
{
    Q_OBJECT
    
public:
    explicit Opcoes(QWidget *parent = 0);
    ~Opcoes();

    int get_tamRam();
    int get_tamVir();
    int get_pageSize();

    void Test();
    
private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_TamVir_editingFinished();

    void on_PageSize_editingFinished();

    void on_TamRam_editingFinished();

private:
    Ui::Opcoes *ui;
};

#endif // OPCOES_H
