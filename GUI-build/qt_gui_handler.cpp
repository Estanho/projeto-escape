#include "qt_gui_handler.h"
#include <QDesktopWidget>
#include <QApplication>

QT_GUI_HANDLER::QT_GUI_HANDLER()
{
}

MemWindow* QT_GUI_HANDLER::mui;
void (*QT_GUI_HANDLER::CBUnReadFunction)(std::string);
bool (*QT_GUI_HANDLER::CBReadFunction)(std::string);
void (*QT_GUI_HANDLER::CBRunFunction)();
bool (*QT_GUI_HANDLER::CBInsertFunction)(std::string);
void (*QT_GUI_HANDLER::CBMountVm)();

long int QT_GUI_HANDLER::tamRam;
long int QT_GUI_HANDLER::tamVir;
long int QT_GUI_HANDLER::tamPaginas;

void QT_GUI_HANDLER::setCenterOfApplication(QWidget *w, bool useSizeHint) {

    if(w->isFullScreen())

        return;



    QSize size;

    if(useSizeHint)

        size = w->sizeHint();

    else

        size = w->size();



    QDesktopWidget *d = QApplication::desktop();

    int ws = d->width();   // returns screen width

    int h = d->height();  // returns screen height

    int mw = size.width();

    int mh = size.height();

    int cw = (ws/2) - (mw/2);

    int ch = (h/2) - (mh/2);

    w->move(cw,ch);

}
