#include "memwindow.h"
#include "ui_memwindow.h"
#include <iostream>
#include "qt_gui_handler.h"

MemWindow::MemWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MemWindow)
{
    ui->setupUi(this);

    pui = NULL;

    QT_GUI_HANDLER::setCenterOfApplication(this, false);
}

MemWindow::~MemWindow() {
    delete ui;
    delete pui;
}

void MemWindow::on_Opcoes_clicked() {
    if(!pui) {
        pui = new Opcoes(this);
        pui->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    }
    pui->Test();
    pui->show();
}

void MemWindow::InsertVirtualLine(QString info) {
    static int row = 0;
    QStringList ss = info.split(' ');

    if(ui->Virtual->rowCount() < row + 1)
        ui->Virtual->setRowCount(row + 1);
    if(ui->Virtual->columnCount() < ss.size())
        ui->Virtual->setColumnCount( ss.size() );

    for( int column = 0; column < ss.size(); column++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem( ss.at(column) );
        ui->Virtual->setItem(row, column, newItem);
    }

    row++;
}

void MemWindow::InsertRealLine(QString info) {
    static int row = 0;
    QStringList ss = info.split(' ');

    if(ui->Real->rowCount() < row + 1)
        ui->Real->setRowCount(row + 1);
    if(ui->Real->columnCount() < ss.size())
        ui->Real->setColumnCount( ss.size() );

    for( int column = 0; column < ss.size(); column++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem( ss.at(column) );
        ui->Real->setItem(row, column, newItem);
    }

    row++;
}

void MemWindow::InsertTabelaLine(QString info) {
    static int row = 0;
    QStringList ss = info.split(' ');

    if(ui->TabelaPaginas->rowCount() < row + 1)
        ui->TabelaPaginas->setRowCount(row + 1);
    if(ui->TabelaPaginas->columnCount() < ss.size())
        ui->TabelaPaginas->setColumnCount( ss.size() );

    for( int column = 0; column < ss.size(); column++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem( ss.at(column) );
        ui->TabelaPaginas->setItem(row, column, newItem);
    }

    row++;
}

void MemWindow::EditVirtualLine(int row, QString info) {
    QTableWidgetItem *item = ui->Virtual->item( row, 1 );

    item->setText(info);

    ui->Virtual->setItem(row, 1, item);
}

void MemWindow::EditRealLine(int row, QString info) {
    QTableWidgetItem *item = ui->Virtual->item( row, 1 );

    item->setText(info);

    ui->Real->setItem(row, 1, item);
}

void MemWindow::EditTabelaLine(int row, QString info) {
    QTableWidgetItem *item = ui->Virtual->item( row, 1 );

    item->setText(info);

    ui->TabelaPaginas->setItem(row, 1, item);
}

void MemWindow::on_actionFechar_triggered() {
    setVisible(false);
}
