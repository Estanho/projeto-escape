#include "opcoes.h"
#include "ui_opcoes.h"
#include "qt_gui_handler.h"

#include <iostream>

Opcoes::Opcoes(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Opcoes)
{
    ui->setupUi(this);

    QT_GUI_HANDLER::tamRam = PADRAO_REAL;
    QT_GUI_HANDLER::tamVir = PADRAO_VIRTUAL;
    QT_GUI_HANDLER::tamPaginas = PADRAO_PAGINA;

    ui->TamRam->insert(QString::number(QT_GUI_HANDLER::tamRam));
    ui->TamVir->insert(QString::number(QT_GUI_HANDLER::tamVir));
    ui->PageSize->insert(QString::number(QT_GUI_HANDLER::tamPaginas));

    QT_GUI_HANDLER::setCenterOfApplication(this, false);
}

Opcoes::~Opcoes()
{
    delete ui;
}

int Opcoes::get_tamRam() {
    return QT_GUI_HANDLER::tamRam;
}

int Opcoes::get_tamVir() {
    return QT_GUI_HANDLER::tamVir;
}

int Opcoes::get_pageSize() {
    return QT_GUI_HANDLER::tamPaginas;
}

void Opcoes::Test() {
    long int auxRam = ui->TamRam->text().toLong();
    long int auxVir = ui->TamVir->text().toLong();
    long int auxPag = ui->PageSize->text().toLong();

    if(auxVir < auxRam) {
        ui->buttonBox->setEnabled(false);
    }
    else if((auxVir % auxPag) != 0) {
        ui->buttonBox->setEnabled(false);
    }
    else if((auxRam % auxPag) != 0) {
        ui->buttonBox->setEnabled(false);
    }
    else {
        ui->buttonBox->setEnabled(true);
    }
}

void Opcoes::on_buttonBox_accepted() {

    QT_GUI_HANDLER::tamRam = ui->TamRam->text().toLong();
    QT_GUI_HANDLER::tamVir = ui->TamVir->text().toLong();
    QT_GUI_HANDLER::tamPaginas = ui->PageSize->text().toLong();

    ui->TamRam->clear();
    ui->TamVir->clear();
    ui->PageSize->clear();

    ui->TamRam->insert(QString::number(QT_GUI_HANDLER::tamRam));
    ui->TamVir->insert(QString::number(QT_GUI_HANDLER::tamVir));
    ui->PageSize->insert(QString::number(QT_GUI_HANDLER::tamPaginas));
}

void Opcoes::on_buttonBox_rejected() {
    ui->TamRam->clear();
    ui->TamVir->clear();
    ui->PageSize->clear();

    ui->TamRam->insert(QString::number(QT_GUI_HANDLER::tamRam));
    ui->TamVir->insert(QString::number(QT_GUI_HANDLER::tamVir));
    ui->PageSize->insert(QString::number(QT_GUI_HANDLER::tamPaginas));
}

void Opcoes::on_TamVir_editingFinished() {
    Test();
}

void Opcoes::on_PageSize_editingFinished() {
    Test();
}

void Opcoes::on_TamRam_editingFinished()
{
    Test();
}
