#include "../includes/TOOLS.h"
#define DEBUG 0

#if CORRECAO_MEMORIA
	int tabs_correcao_memoria = 0;
	unsigned _tamPagina_;
	
	void print_tabs()
	{
	    for(int i = 0 ; i < tabs_correcao_memoria ; i++)
	        printf("\t");
	}
#endif

bool comp(const char* s1, const char* s2) {
    char s3[strlen(s1)];
    sscanf(s1, "%s", s3);

#ifdef __linux__
    return strcasecmp(s3, s2);
#elif defined _WIN32 || defined _WIN64
    return _stricmp(s3, s2);
#endif

}

char* strtolower( char * str)
{
	char * _str = str;
	while( str[0] != '\0' )
	{
		if( str[0] >= 'A' && str[0] <= 'Z' )
		{
			str[0] = str[0] - 'A' + 'a' ; 
		}
		str++;
	}
	return _str;
}

Exception::Exception(const int i, const char *f, const char *text){
	std::string str1(f);
	int loc = str1.find_last_of("/");
	std::string str2 = str1.substr(loc+1, str1.size()-loc);
	sprintf(exceptionMsg,"\n\nAn exception has been thrown in \nFile:\t%s\n"
	"Line:\t%d\n"
	"Cause:\t%s\n",str2.c_str(),i,text);
	showExceptionMessage();
}

void Exception::showExceptionMessage() const{
	// print message on screen
	std::cout << exceptionMsg << std::endl;
	// print message on file
	std::ofstream fid;
	fid.open("ExceptionThrown.log");
	fid << "Application has been terminated by an exception throw:\n\n";
	fid << exceptionMsg << std::endl;
}

Exception::~Exception(){
}

void removeColchete( string& str )
{
	#if DEBUG 
	cout << "removeColchete(): -" <<str <<"-"<< endl;
	#endif 
	str.erase(0,1);
	str.erase(str.size()-1,1);
	#if DEBUG 
	cout << "removeColchete(): -" <<str <<"-"<< endl;
	#endif 
}

#undef DEBUG 